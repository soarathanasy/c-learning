// 1. 普通的命名空间
namespace N1 { // N1为命名空间的名称
    // 命名空间中的内容: 可定义 (变量 / 函数)
    int a;
    int Add(int left, int right) {
        return left + right;
    }
}

// 2. 命名空间可嵌套
namespace N2 {
    int a, b;
    int Add(int left, int right) {
        return left + right;
    }
    namespace N3 {
        int c, d;
        int sub(int left, int right) {
            return left - right;
        }
    }
}

/* 3. 同一个工程中允许存在多个相同名称的命名空间，
      编译器最终会合成到同一个命名空间中   */
namespace N1 {
    int mul(int num1, int num2) {
        return num1 * num2;
    }
}

